sudo apt update
 1998  sudo apt install snapd
 1999  snap find
 2000  sudo snap install microk8s --classic
 2001  microk8s status --wait-ready
 2002  sudo usermod -a -G microk8s radovan
 2003  sudo chown -f -R radovan ~/.kube
 2004  microk8s enable dashboard dns registry istio
 2005  sudo microk8s enable dashboard dns registry istio
 2006  pwd
 2007  cd Documents/
 2008  gedit nginx_deployment.yml
------------------------------------------------------------------------------------
1997  microk8s kubectl get svc -n kube-system
 1998  sudo microk8s kubectl get svc -n kube-system
 1999  sudo microk8s kubectl get pods
 2000  alias k="sudo microk8s kubectl"
 2001  k
 2002  k run nginx --image=nginx --port=80
 2003  k get pods
 2004  k run nginx --image=nginx --port=80 --dry-run=client -o yaml
 2005  k get pods
 2006  k port-forward po/nginx 80:80
 2007  k port-forward po/nginx 8085:80
 2008  k get pods
 2009  pwd
 2010  cd Documents/
 2011  ll
 2012  k create -f nginx_deployment.yml
 2013  k get deploy
 2014  k get rs
 2015  k get po
 2016  k get deploy
 2017  k get po
 2018  k get rs
 2019  gedit nginx_deployment.yml
 2020  k apply -f nginx_deployment.yml
 2021  k get rs
 2022  k get po
 2023  k get deployment -o yaml
 2024  k get deployment -o yaml | grep strategy
 2025  k get deployment -o yaml | grep strategy -A5
 2026  k rollout history deployment frontend
 2027  k get deployment
 2028  k rollout history deployment nginx-deployment
 2029  k rollout undo deploy nginx-deployment
 2030  k get deployment -o yaml | grep strategy -A5
 2031  k get deployment -o yaml | grep image -A5
 2032  k rollout history deployment nginx-deployment
 2033  k set image deploy nginx-deployment nginx=nginx:latest
 2034  k rollout history deployment nginx-deployment
 2035  k get deployment -o yaml | grep image -A5
 2036  k rollout history deployment nginx-deployment
 2037* k rollout undo
 2038  k rollout history deployment nginx-deployment --to-revision=2 --record
 2039  k rollout undo deployment nginx-deployment --to-revision=2 --record
 2040  k rollout undo deployment nginx-deployment --to-revision=2
 2041  k get deployment -o yaml | grep image -A5
 2042  k create -f nginx_deployment.yml
 2043  k get deploy
 2044  k get deploy -l app=tayara
 2045  k get deploy -l app=nginx
 2046  k create ns frontend
 2047  k create ns backend
 2048  k get ns
 2049  k create -f nginx_deployment.yml -n frontend
 2050  k create -f nginx_deployment.yml -n backend
 2051  k get deploy
 2052  k get deploy -a
 2053  k get deploy -A
 2054  k get po -A -O wide
 2055  k get po -A -o wide
 2056  k get po --show-labels
 2057  k get deploy -n frontend --show-labels
 2058  k label test-nginx-deployment app=jumia
 2059  k label test-nginx-deployment app=jumia -n frontend
 2060  k label deploy test-nginx-deployment app=jumia -n frontend
 2061  k label deploy test-nginx-deployment app=jumia -n frontend --overwrite
 2062  k get deploy -n frontend --show-labels
 2063  history
------------------------------------------------------------------------------
token=$(microk8s kubectl -n kube-system get secret | grep default-token | cut -d " " -f1)
 1998  microk8s kubectl
 1999  token=$(microk8s kubectl -n kube-system get secret | grep default-token | cut -d " " -f1) microk8s kubectl -n kube-system describe secret $token
 2000  microk8s kubectl port-forward -n kube-system service/kubernetes-dashboard 10443:443
----------------------------------------------------------------------------------------------------------------