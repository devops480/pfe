### synchronize your code with your team.
### send terminal theme to Nouha
### namespace:


- Create namespace (imperative method):
```
kubectl create ns $namespace_name
```
- Create namespace (declarative mode):

##### how generate namespace.yaml file ?
```
kubectl create ns ${namespace_name} --dry-run=client -o yaml > namespace.yaml
```

* namespace.yaml
```yaml
apiVersion: v1
kind: Namespace
metadata:
  creationTimestamp: null
  name: front-end-ns-test
spec: {}
status: {}
```

and now execute the command:

```
kubectl create -f namespace.yaml
```

- Get namespace:
```
kubectl get ns
```
- Get ressources on specific namespace :

```
Kubectl get all ,po,deploy,rs -n $namespace_name
```
- Create k8s Object on a specific namespace:

```
kubectl create ${object_type} -n ${namespace_name}
```

```
  clusterrole         Create a ClusterRole.
  clusterrolebinding  Create a ClusterRoleBinding for a particular ClusterRole
  configmap           Create a configmap from a local file, directory or literal value
  cronjob             Create a cronjob with the specified name.
  deployment          Create a deployment with the specified name.
  job                 Create a job with the specified name.
  namespace           Create a namespace with the specified name
  poddisruptionbudget Create a pod disruption budget with the specified name.
  priorityclass       Create a priorityclass with the specified name.
  quota               Create a quota with the specified name.
  role                Create a role with single rule.
  rolebinding         Create a RoleBinding for a particular Role or ClusterRole
  secret              Create a secret using specified subcommand
  service             Create a service using specified subcommand.
  serviceaccount      Create a service account with the specified name
  ```

  ##### Get Objects From a namespace
  ```
  kubectl get all -n ${namespace_name}
  ```

### label, selector


- Add label to k8s objects:

```
kubectl run my-front-app --image=nginx --labels="app=myapp" --dry-run=client -o yaml > nginx.yaml
```


- Add labels after objects creation:

- edit object : set , edit, label

- kubectl label pods my-front-app -n frontend-ns unhealthy=true

kubectl label po/front-app -n frontend-ns unhealthy=true --overwrite

kubectl get po -n frontend-ns --selector="app=myapp,unhealthy=true"

kubectl get po -n frontend-ns -l="app=myapp,unhealthy=true"



### Service:


Type1:   clusterIP


namespace.servicename.default

Type2:   NodePort

Type3:   LoadBalancer



### ConfigMap


### Secret




       kubectl get ns
 2024  k create ns frontend-ns
 2025  k get ns | grep frontend-ns
 2026  k get ns | grep backend-ns
 2027  k create ns backend-ns
 2028  k get ns | grep -ns
 2029  k get ns | grep ns
 2030  k run front-app --image=nginx -n frontend-ns
 2031  k run back-app --image=nginx -n backend-ns
 2032  k get po -n frontend-ns
 2033  k get po -n backend-ns
 2034  kubectl port-forward --help
 2035  kubectl port-forward pod/front-app 8082:80 -n frontend-ns
 2036  cd /home/rassakra/Desktop/mean-stack-docker/deployment
 2037  # kubectl run my-front-app --image=nginx -
 2038  kubectl run --help
 2039  kubectl run my-front-app --image=nginx  --labels='app=myapp' --dry-run=client -o yaml > nginx.yaml
 2040  kubectl create -f nginx.yaml -n frontend-ns
 2041  k get po -n frontend-ns
 2042  kubectl exec --help
 2043  kubectl exec my-front-app -i -t -- sh
 2044  kubectl exec my-front-app -n frontend-ns -i -t -- sh
 2045  kubectl port-forward my-front-app -n frontend-ns 8082:80
 2046  kubectl label --help
 2047  kubectl label pods my-front-app -n frontend-ns unhealthy=true
 2048  kubectl get po -n my-front-app --show-labels
 2049  kubectl get po -n frontend-ns --show-labels
 2050  kubectl get po -n frontend-ns --show-labels -o yaml
 2051  kubectl get po/my-front-app -n frontend-ns -o yaml
 2052  kubectl get po -n frontend-ns --show-labels
 2053  kubectl label po/front-app -n frontend-ns unhealthy=true
 2054  kubectl get po -n frontend-ns --show-labels
 2055  kubectl label po/front-app -n frontend-ns unhealthy=false
 2056  kubectl label po/front-app -n frontend-ns unhealthy=false --overwrite
 2057  kubectl get po -n frontend-ns --show-labels
 2058  k get po -n frontend-ns -l unhealthy=false
 2059  k get po -n frontend-ns -l unhealthy=true
 2060  kubectl label po/front-app -n frontend-ns unhealthy=true --overwrite
 2061  kubectl get po -n frontend-ns --show-labels
 2062  kubectl get po -n frontend-ns --label="app=myapp,unhealthy=true"
 2063  kubectl get po -n frontend-ns --selector="app=myapp,unhealthy=true"
 2064  kubectl get po -n frontend-ns -l="app=myapp,unhealthy=true"
 2065  kubectl create -f service.yaml
 2066  kubectl create -f service.yaml  -n frontend-ns
 2067  kubectl get svc -n frontend-ns
 2068  kubectl get svc -n frontend-ns -o yaml
 2069  kubectl apply -f service.yaml  -n frontend-ns
 2070  kubectl get svc -n frontend-ns -o yaml




---



env variable



ConfigMap





Secret


ressources